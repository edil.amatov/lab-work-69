def max_number(item)
	maximum = 0
	item.each do |i|
		if maximum < i
			maximum = i
		end
	end
	maximum
end

def position_of_max_number(max_number, array_of_numbers)
	positions_number = []
	array_of_numbers.size.times do |i| 
		if max_number == array_of_numbers[i]
			positions_number.push(i)
		end
	end	
	positions_number
end

array_of_numbers = []
10.times do 
	array_of_numbers.push(rand(0..9))
end
p array_of_numbers
max_number = max_number(array_of_numbers)
p "Max = #{max_number}"
positions = position_of_max_number(max_number,array_of_numbers)
quatity_of_numbers_before_max = max_number(positions)
p "Количество элементов перед последним максимальным = #{quatity_of_numbers_before_max}"