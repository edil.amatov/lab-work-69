require_relative 'task4.rb'
require "test/unit"

class TestTask4 < Test::Unit::TestCase

	def test_for_method_max_number
		arr = [1,2,3,4,5,6,7,8,9]
		max_result = max_number(arr)
		assert_equal 9, max_result
	end

	def test_for_position_of_max_number
		arr = [1,2,3,4,5,6,7,8,9]
		assert_equal [8], position_of_max_number(9,arr)
	end
end