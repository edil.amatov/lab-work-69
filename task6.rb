arr = [1,3,5,7,9]
def arithmetic_progression?(arr) 
    return true if arr.size < 2 
    (arr.first..arr.last).step((arr.last-arr.first)/(arr.size-1)).to_a == arr 
end 

if arithmetic_progression?(arr) == true
	avg_interval = (arr.last-arr.first)/(arr.size-1) 
	p avg_interval
else
	p nil
end